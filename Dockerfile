from ubuntu:18.04

RUN apt update && apt upgrade -y
RUN apt install -y build-essential 
RUN apt install -y libssl-dev
RUN apt install -y libxml2-dev
RUN apt install -y libxslt1-dev
RUN apt install -y libbz2-dev
RUN apt install -y zlib1g-dev
RUN apt install -y python-setuptools
RUN apt install -y python-dev
RUN apt install -y python-virtualenv
RUN apt install -y libjpeg62-dev
RUN apt install -y libreadline-gplv2-dev
RUN apt install -y python-pil
RUN apt install -y wv
RUN apt install -y poppler-utils
RUN apt install -y git

# Ambiente de produção
## Criar Usuário
RUN useradd --system --shell /bin/bash --comment 'Plone Administrator' \
--user-group -m --home-dir /opt/plone plone
RUN passwd plone
USER plone

#Instalando o Portal
## Usando repositório
WORKDIR ~
RUN git clone https://github.com/plonegovbr/portal.buildout.git

